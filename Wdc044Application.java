package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
// Tells the spring boot that this will handle endpoints for web request.
@RestController
public class Wdc044Application {

    public static void main(String[] args) {
        SpringApplication.run(Wdc044Application.class, args);
    }

    // This is used for mapping HTTP GET requests.
    @GetMapping("/hello")
    // "@RequestParam" is used to extract query parameters
        // name = john; Hello john.
        // name = World; Hello World.
    public String hello(@RequestParam(value = "name", defaultValue = "World" ) String name){
        return String.format("Hello %s", name);
    }
    
    public String hi(@RequestParam(value = "name", defaultValue = "user" ) String name){
	return String.format("Hi %s", name);
    }


}
